package com.example.cloud;

import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class ImageService {

    final FileProvider fileProvider;

    public ImageService(FileProvider fileProvider) {
        this.fileProvider = fileProvider;
    }

    List<File> fetchImages(String path) {
        return fileProvider.fetchAllFiles(path).stream()
                .filter(file -> Pattern.matches(".*\\.(jpeg|jpg)", file.getName()))
                .collect(Collectors.toList());
    }
}
