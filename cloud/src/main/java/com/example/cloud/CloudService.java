package com.example.cloud;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.regex.Pattern;

@Service
public class CloudService {

    private final ImageService imageService;
    private final AmazonS3 amazonS3;

    public CloudService(ImageService imageService, AmazonS3 amazonS3) {
        this.imageService = imageService;
        this.amazonS3 = amazonS3;
    }

    void uploadImages(String albumName, String path) {
        imageService.fetchImages(path).forEach(
                image -> amazonS3.putObject(CloudConfig.BUCKET, albumName + "/" + image.getName(), image)
        );
    }

    void downloadImagesFromAlbum(String album, String path) {
        amazonS3.listObjects("sakaev-test", album).getObjectSummaries().stream()
                .filter(key -> Pattern.matches(".*/.*", key.getKey()))
                .forEach(key -> {

                    amazonS3.getObject(new GetObjectRequest(CloudConfig.BUCKET, key.getKey()), new File(path + "\\" + key.getKey().split("/")[1]));
                });
    }

    void listAlbums() {
        amazonS3.listObjects(CloudConfig.BUCKET).getObjectSummaries().stream()
                .map(S3ObjectSummary::getKey)
                .filter(key -> Pattern.matches(".*/.*", key))
                .map(key -> key.split("/")[0])
                .distinct()
                .forEach(System.out::println);
    }

    void listAlbumImages(String album) {
        if (amazonS3.listObjects(CloudConfig.BUCKET, album).getObjectSummaries().stream().toArray().length == 0) {
            System.out.println("такого альбома нет");
            return;
        }
        amazonS3.listObjects(CloudConfig.BUCKET, album).getObjectSummaries().stream()
                .map(S3ObjectSummary::getKey)
                .filter(key -> Pattern.matches(".*/.*", key))
                .map(key -> key.split("/")[1])
                .forEach(System.out::println);
    }
}
