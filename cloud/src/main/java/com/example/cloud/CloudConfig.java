package com.example.cloud;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

@Configuration
public class CloudConfig {

    static String BUCKET = null;

    @Bean
    public AmazonS3 amazonS3() throws FileNotFoundException {
        String accessKey = null;
        String secretKey = null;
        try(BufferedReader br = new BufferedReader(new FileReader("text.txt"))) {
            accessKey = br.readLine();
            secretKey = br.readLine();
            BUCKET = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String finalAccessKey = accessKey;
        String finalSecretKey = secretKey;
        AmazonS3 s3 = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new AWSCredentials() {
                    @Override
                    public String getAWSAccessKeyId() {
                        return finalAccessKey;
                    }

                    @Override
                    public String getAWSSecretKey() {
                        return finalSecretKey;
                    }
                }))
                .withEndpointConfiguration(
                        new AmazonS3ClientBuilder.EndpointConfiguration(
                                "storage.yandexcloud.net", "ru-central1"
                        )
                )
                .build();
        if (!s3.doesBucketExistV2(BUCKET)) {
            s3.createBucket(BUCKET);
        }
        return s3;
    }
}
