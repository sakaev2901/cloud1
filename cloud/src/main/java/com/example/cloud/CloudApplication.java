package com.example.cloud;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

@SpringBootApplication
public class CloudApplication {

    public static void main(String[] args) throws IOException {
        ApplicationContext context = SpringApplication.run(CloudApplication.class, args);

        CloudService cloudService = context.getBean(CloudService.class);

        File file = new File("text.txt");
        System.out.println(file.exists());

        if (args.length == 0) {
            System.out.println("Укажите команду");
        }
        System.out.println(args.length);
        switch (args[0]) {
            case "upload": {
                String album = null;
                String path = null;
                if (args.length != 5) {
                    System.out.println("Недостаточно парамтреов");
                } else {
                    if (args[1].equals("-p")) {
                        path = args[2];
                    } else if (args[1].equals("-a")) {
                        album = args[2];
                    }
                    if (args[3].equals("-p")) {
                        path = args[4];
                    } else if (args[3].equals("-a")) {
                        album = args[4];
                    }
                }

                cloudService.uploadImages(album, path);

            }
            break;
            case "download": {
                String album = null;
                String path = null;
                if (args.length != 5) {
                    System.out.println("Недостаточно парамтреов");
                } else {
                    if (args[1].equals("-p")) {
                        path = args[2];
                    } else if (args[1].equals("-a")) {
                        album = args[2];
                    }
                    if (args[3].equals("-p")) {
                        path = args[4];
                    } else if (args[3].equals("-a")) {
                        album = args[4];
                    }
                }

                cloudService.downloadImagesFromAlbum(album, path);

            }
            break;
            case "list": {
                String album = null;
                if (args.length == 3) {
                    if (args[1].equals("-a")) {
                        album = args[2];
                    }
                    if (album == null) {
                        System.out.println("Введите название альбома");
                        return;
                    }
                    cloudService.listAlbumImages(album);
                } else {
                    cloudService.listAlbums();
                }
            }
            break;

            default: {
                System.out.println("Неизевстная команда");
                return;
            }
        }


//        s3.putObject("sakaev-test", "s/aaa1", resource);

//        s3.listObjects("sakaev-test").getObjectSummaries().forEach(f -> System.out.println(f));

//        ImageService imageService = context.getBean(ImageService.class);
//        cloudService.uploadImages("test3", "C:\\Users\\LDAR\\Desktop\\images");
//        cloudService.listAlbums();
//        cloudService.listAlbumImages("test3");
//        cloudService.downloadImagesFromAlbum("test3", "C:\\Users\\LDAR\\Desktop\\images\\test");
//        imageService.fetchImages("C:\\Work\\cloud1\\cloud");

    }

}
