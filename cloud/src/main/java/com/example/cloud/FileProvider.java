package com.example.cloud;

import org.springframework.stereotype.Repository;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Repository
public class FileProvider {

    List<File> fetchAllFiles(String path) {
        File file = new File(path);
        return Arrays.asList(Objects.requireNonNull(file.listFiles()));
    }
}
